import Head from 'next/head'
import Image from 'next/image'
import Cards from '../components/Cards'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Maincontent from '../components/maincontent'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Home</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
          <Header/>
          <Maincontent/>
          <Cards/>
          <Footer/>
    </div>
  )
}
