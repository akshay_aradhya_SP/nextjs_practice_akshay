import React from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import Icon from '@mdi/react';
import { mdiTwitter,mdiFacebook,mdiBaseball,mdiLinkedin } from '@mdi/js';

/*
  @@footer 
  @@component
*/

const Footer =()=>{
    return(
        <>
        <div className="footer_container">
        <Container className="footer_custom">
            <Row>
                <Col md={6} xs={6} className="col_gutter">
                <div className="footer_main_font">ABOUT</div>
                upGrad Jeet aligns with the brand’s ambition of Rural India penetration strategy in making Bharat employable. upGrad Jeet aims to ensure the win (“Jeet”) of every learner. Winners need to sweat it out every day to win and the course is exactly designed to make every aspirant become an active learner. With daily and weekly goal settings, notification-based study reminders, one-to-one mentorship, 24/7 doubt solving, revision modules, personalization all driven by technology, upGrad Jeet stands for participative learning. We have the best faculties to teach and make you solve thousands of questions while you learn from basic concepts to mix, and complex ones to ultimately solve questions at the actual exam level and beyond. The learning methodology is like a guided tour where each and every learner is supported according to their level.
                </Col>
                <Col md={3} className="col_gutter">
                <div className="footer_main_font">ONLINE COACHING</div>
                <a href="www.google.com" className="footer_link">Coaching for IBPS PO Exam</a><br/>
                <a href="www.google.com" className="footer_link">Coaching for SSC CGL Exam</a><br/>
                <a href="www.google.com" className="footer_link">Coaching for SSC CHSL Exam</a><br/>
                <a href="www.google.com" className="footer_link">Coaching for NRA CET Exam</a>
                </Col>
                <Col md={3} className="col_gutter">
                    <div className="footer_main_font">COMPANY</div>
                    UpGrad Jeet<br/>
                    3rd Floor, Novel Tech Park, #46/4, Kudlu <br/>
                    Gate, Hosur Rd,<br/>
                    HSR Extension, Bengaluru, Karnataka 560068<br/>
                    Ph: +91 8040 611 000
                </Col>
            </Row>
            <div className="line"></div>
            <div className="footer_copyrights">
                <div className="footer_social_icons_holder">
                      <p>Copyright © 2021 All Rights Reserved by  UpGrad Jeet.</p>
                </div>
                <div >
                <Icon className="social_media_icons" path={mdiFacebook}
                    size={1}
                    />
                <Icon className="social_media_icons" path={mdiTwitter}
                    size={1}
                    />
                <Icon className="social_media_icons" path={mdiBaseball}
                    size={1}
                    />
                <Icon className="social_media_icons" path={mdiLinkedin}
                    size={1}
                    />
                </div>
            </div>
       </Container>
       </div>   
        </>
    );
}

export default Footer;