import React, { Component } from 'react';
import { Dropdown } from 'react-bootstrap';
import MoreOptions from './MoreOptions';

/*
  @@ class component

  used map function for drop down as it is different kind of approach in class based component
  where we cant use declare variables directly

  for drop down tried to pass component to job done and im bit strucked with styles
*/

class Header extends Component{

    constructor(props) {
        super(props);
        this.state={
            dropdown:[{
                text:"Login"
            },
             {
                 text:"Cart"
             },
            {
                text:'Notifications'
            },
            {
                text:'Switch Language'
            }
        ]
        }
    }

    render(){
        
    return (
        <header className='navbar'>
            <div className='navbar__title navbar__item'>
                <span><img className="Navbarimage" src="/Assets/images/logo.png" alt="logo"/></span>
                <span className="text_navbar">MY PROGRAM</span>
                </div>
            <div className='navbar__item'><img className="Navbarimage nav_icons_cart" src="/Assets/images/cart.PNG" alt="logo"/></div>
            <div className='navbar__item'><img className="Navbarimage nav_icons_bell" src="/Assets/images/bell.PNG" alt="logo"/></div>
            <div className='navbar__item'><b className="Navbar_login">LOGIN</b></div> 
            <div className='navbar__item'><b className="Navbar_more">
            <Dropdown size="550px">
                <Dropdown.Toggle as={MoreOptions}>
                   
                </Dropdown.Toggle>

                <Dropdown.Menu>
                {this.state.dropdown.map((val,index)=>(
                                <Dropdown.Item key={index} style={{postion:'relative',fontSize:'13px',marginBottom:'10px',padding:'10px 20px'}}>
                                    {val.text}
                                    <span style={{position:'absolute',left:'0'}} className="line_new">
                                        </span>
                                </Dropdown.Item>
                            ))
                }
                </Dropdown.Menu>
                </Dropdown>

               </b>
               </div>           
        </header>
      );
    }
  };

export default Header;

