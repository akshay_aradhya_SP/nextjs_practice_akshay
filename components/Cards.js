import React from 'react';
import { Container, Col, Row, Card, Button } from 'react-bootstrap' ;
import styles from '../styles/cards.module.css';

/*
 cards can be mapped by defining an object
 cards=[
     {
         heading:"card heading",
         content:"card content",
         btntxt:"text"
     }
 ]

 return(
     cards.map((val,index)=>{
         <>..key={index}
            {val.heading}
            {val.content}
         </>
     })
 )

 just for simplicity Hardcodded
*/

const Cards = () =>{
    return(
        <>
           <div className="text-center">
           <h2 className={styles.card_header}>Our Online Courses</h2>
           <p className={styles.card_text}>Comprehensive Online Learning Platform with Students-Centric Coaching for aspirants of government job exams in India. All our Courses are structured & aligned with exam syllabus to help you best prepare for it. Our Program is powered by India's Awarded Coaching Institute to Make Your DREAM come TRUE.
           </p></div>
        <Container style={{paddingTop:'15px',paddingBottom:'15px'}} >
            <Row>
                <Col md={4} className={styles.col_gutter}>
                    <Card>
                        <Card.Img variant="top" src="/Assets/images/cardimage.jpg" />
                        <Card.Body>
                            <Card.Title>Banking Exams</Card.Title>
                            <Card.Text>

                                <span className={styles.card_secondary_font}>
                                In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain. Every year lakhs of aspirants appear for bank exams, but very handful of them clears it. Candidates can start their preparation for upcoming exams with this Comprehensive Learning Program .
                                </span>
                            
                            </Card.Text>
                            <Button variant="outline-danger" style={{marginTop:'30px'}}>KNOW MORE</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={4} className={styles.col_gutter}> 
                <Card >
                        <Card.Img variant="top" src="/Assets/images/cardimage.jpg" />
                        <Card.Body>
                            <Card.Title>Banking Exams</Card.Title>
                            <Card.Text>

                                <span className={styles.card_secondary_font}>
                                In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain. Every year lakhs of aspirants appear for bank exams, but very handful of them clears it. Candidates can start their preparation for upcoming exams with this Comprehensive Learning Program .
                                </span>
                            
                            </Card.Text>
                            <Button variant="outline-danger" style={{marginTop:'30px'}}>KNOW MORE</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={4} className={styles.col_gutter}>
                <Card >
                        <Card.Img variant="top" src="/Assets/images/cardimage.jpg"/>
                        <Card.Body >
                            <Card.Title>Banking Exams</Card.Title>
                            <Card.Text >
                
                                <span  className={styles.card_secondary_font}>
                                In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain. Every year lakhs of aspirants appear for bank exams, but very handful of them clears it. Candidates can start their preparation for upcoming exams with this Comprehensive Learning Program .
                                </span>
                            
                            </Card.Text>
                            <Button variant="outline-danger" style={{marginTop:'30px'}}>KNOW MORE</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </>
    );
}

export default Cards;