import React from 'react';
import styles from '../styles/mainslider.module.css';

 const Maincontent = () =>{
    return(
       <>
       <div className={styles.banner_holder}>
         <img src="/Assets/images/banner.PNG" alt="banner"/>
       </div>
       <div className={styles.banner_content}>
             <h1 className={styles.banner_header}>#1 Trusted Brand for Banking<br/> Exam Preparation</h1>
             <p className={styles.banner_text}>Comprehensive bank coaching program for bank exam preparation with all practice <br/>
             materials in one place. Start Banking Preparation with 500+ Video Lessons, 450+ Tests following latest exam pattern <br/>
             with LIVE and In-App Doubt Solving</p>
             <button className={styles.banner_learnmore}>LEARN MORE   ></button> 
             <button className={styles.banner_watchvideo}>watch <br/>intro video</button>
         </div>
       </>
    );
}

export default Maincontent;

