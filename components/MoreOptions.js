import React from 'react';
import Icon from '@mdi/react';
import { mdiChevronDown } from '@mdi/js';

const MoreOptions = React.forwardRef(({ children, onClick }, ref) => (
    <span
      style={{fontSize:'15px',fontWeight:'500'}}
      ref={ref}
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <span>More Options <Icon size={1} path={mdiChevronDown}/></span>
    </span>
  ));

export default MoreOptions;